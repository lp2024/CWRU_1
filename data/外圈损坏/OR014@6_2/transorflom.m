clc;clear;close all;
load 199.mat

data199_BA = fopen('data199_BA.txt','w');
for i = 1:length(X199_BA_time)
    fprintf(data199_BA,'%.15f\n',X199_BA_time(i));
end

data199_DE = fopen('data199_DE.txt','w');
for i = 1:length(X199_DE_time)
    fprintf(data199_DE,'%.15f\n',X199_DE_time(i));
end


data199_FE = fopen('data199_FE.txt','w');
for i = 1:length(X199_FE_time)
    fprintf(data199_FE,'%.15f\n',X199_FE_time(i));
end


data199_RPM = fopen('data199_RPM.txt','w');
for i = 1:length(X199RPM)
    fprintf(data199_RPM,'%d\n',X199RPM(i));
end