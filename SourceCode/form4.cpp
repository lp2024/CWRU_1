#include "form4.h"
#include "ui_form4.h"
#include <QDesktopServices>
#include <math.h>
#include <string.h>
#include<vector>

// 函数声明
double peak(QList<double> ndata);
double rmf(QList<double> ndata);
double r(QList<double> ndata);
double mean(QList<double> ndata);
double Kurtosis_Value(QList<double> ndata);
double Crest_Factor(QList<double> ndata);
double Clearance_Factor(QList<double> ndata);
double Impulse_Factor(QList<double> ndata);
double Shape_Factor(QList<double> ndata);

form4::form4(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form4){
    ui->setupUi(this);
    eCreatChart();
}

form4::~form4(){
    delete ui;
}

void form4::on_txt_button1_clicked()
{
    ndata1.clear();
    nfileName1 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (nfileName1.isEmpty())     //如果未选择文件便确认，即返回
        return;
    QFile file(nfileName1);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "成功读取数据！", QMessageBox::Ok | QMessageBox::Default , 0 );

        while (!file.atEnd())
        {
          QByteArray line = file.readLine();
          QString str = QString(line);
          QStringList strlist=str.split("\n");  //按照空格分割
          double newdata = strlist[0].toDouble();
          ndata1<<newdata;
        }
        file.close();
        ui->txt1->setCheckState(Qt::Checked);
    }
}
void form4::on_clear1_clicked(){
    nfileName1 = "NULL";
    ndata1.clear();
    memset(nlable1, 0, sizeof (nlable1));       // 数组全部归零
    ui->txt1->setCheckState(Qt::Unchecked);
    ui->lable1->setCheckState(Qt::Unchecked);
}
void form4::on_label_button1_clicked(){
    if (ndata1.size() == 0){
        QMessageBox::warning(0 , "错误" , "请先选择数据", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    int a = 0;
    int b = 0;
    if(ui->comboBox1->currentIndex()==1) b = 1;
    else if(ui->comboBox1->currentIndex()==2) a = 1;
    else if(ui->comboBox1->currentIndex()==3) a = b = 1;
    //qDebug()<<a<<b;
    int n = ndata1.size();
    int step = n / lablenum ;
    for(int i = 0; i < lablenum; i++){
        QList<double> alldata;
        for(int k = i*step; k <(i+1)*step; k++) alldata.append(ndata1[k]);
        //结果保留2位小数
        nlable1[i][0] = ((int)(Kurtosis_Value(alldata) * 100 + 0.5))/100.0;
        nlable1[i][1] = ((int)(Crest_Factor(alldata) * 100 + 0.5))/100.0;
        nlable1[i][2] = ((int)(Clearance_Factor(alldata) * 100 + 0.5))/100.0;
        nlable1[i][3] = ((int)(Impulse_Factor(alldata) * 100 + 0.5))/100.0;
        nlable1[i][4] = ((int)(Shape_Factor(alldata) * 100 + 0.5))/100.0;
        nlable1[i][5] = a;
        nlable1[i][6] = b;
    }
    ui->lable1->setCheckState(Qt::Checked);
}


void form4::on_txt_button2_clicked()
{
    ndata2.clear();
    nfileName2 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (nfileName2.isEmpty())     //如果未选择文件便确认，即返回
        return;
    QFile file(nfileName2);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "成功读取数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        while (!file.atEnd())
        {
          QByteArray line = file.readLine();
          QString str = QString(line);
          QStringList strlist=str.split("\n");  //按照空格分割
          double newdata = strlist[0].toDouble();
          ndata2<<newdata;
        }
        file.close();
        ui->txt2->setCheckState(Qt::Checked);
    }
}
void form4::on_clear2_clicked(){
    nfileName2 = "NULL";
    ndata2.clear();
    memset(nlable2, 0, sizeof (nlable2));       // 数组全部归零
    ui->txt2->setCheckState(Qt::Unchecked);
    ui->lable2->setCheckState(Qt::Unchecked);
}
void form4::on_label_button2_clicked(){
    if (ndata2.size() == 0){
        QMessageBox::warning(0 , "错误" , "请先选择数据", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    int a = 0;
    int b = 0;
    if(ui->comboBox2->currentIndex()==1) b = 1;
    else if(ui->comboBox2->currentIndex()==2) a = 1;
    else if(ui->comboBox2->currentIndex()==3) a = b = 1;
    //qDebug()<<a<<b;
    int n = ndata2.size();
    int step = n / lablenum ;
    for(int i = 0; i < lablenum; i++){
        QList<double> alldata;
        for(int k = i*step; k <(i+1)*step; k++) alldata.append(ndata2[k]);
        //结果保留2位小数
        nlable2[i][0] = ((int)(Kurtosis_Value(alldata) * 100 + 0.5))/100.0;
        nlable2[i][1] = ((int)(Crest_Factor(alldata) * 100 + 0.5))/100.0;
        nlable2[i][2] = ((int)(Clearance_Factor(alldata) * 100 + 0.5))/100.0;
        nlable2[i][3] = ((int)(Impulse_Factor(alldata) * 100 + 0.5))/100.0;
        nlable2[i][4] = ((int)(Shape_Factor(alldata) * 100 + 0.5))/100.0;
        nlable2[i][5] = a;
        nlable2[i][6] = b;
    }
    ui->lable2->setCheckState(Qt::Checked);
}


void form4::on_txt_button3_clicked()
{

    nfileName3 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (nfileName3.isEmpty()) return;  //如果未选择文件便确认，即返回

    ndata3.clear();
    QFile file(nfileName3);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "成功读取数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        while (!file.atEnd()){
          QByteArray line = file.readLine();
          QString str = QString(line);
          QStringList strlist=str.split("\n");  //按照空格分割
          double newdata = strlist[0].toDouble();
          ndata3<<newdata;
        }
        file.close();
        ui->txt3->setCheckState(Qt::Checked);
    }
}
void form4::on_clear3_clicked(){
    nfileName3 = "NULL";
    ndata3.clear();
    memset(nlable3, 0, sizeof (nlable3));       // 数组全部归零
    ui->txt3->setCheckState(Qt::Unchecked);
    ui->lable3->setCheckState(Qt::Unchecked);
}
void form4::on_label_button3_clicked(){
    if (ndata3.size() == 0){
        QMessageBox::warning(0 , "错误" , "请先选择数据", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    int a = 0;
    int b = 0;
    if(ui->comboBox3->currentIndex()==1) b = 1;
    else if(ui->comboBox3->currentIndex()==2) a = 1;
    else if(ui->comboBox3->currentIndex()==3) a = b = 1;
    //qDebug()<<a<<b;
    int n = ndata3.size();
    int step = n / lablenum ;
    for(int i = 0; i < lablenum; i++){
        QList<double> alldata;
        for(int k = i*step; k <(i+1)*step; k++) alldata.append(ndata3[k]);
        //结果保留2位小数
        nlable3[i][0] = ((int)(Kurtosis_Value(alldata) * 100 + 0.5))/100.0;
        nlable3[i][1] = ((int)(Crest_Factor(alldata) * 100 + 0.5))/100.0;
        nlable3[i][2] = ((int)(Clearance_Factor(alldata) * 100 + 0.5))/100.0;
        nlable3[i][3] = ((int)(Impulse_Factor(alldata) * 100 + 0.5))/100.0;
        nlable3[i][4] = ((int)(Shape_Factor(alldata) * 100 + 0.5))/100.0;
        nlable3[i][5] = a;
        nlable3[i][6] = b;
    }
    ui->lable3->setCheckState(Qt::Checked);
}


void form4::on_txt_button4_clicked()
{
    nfileName4 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (nfileName4.isEmpty())     //如果未选择文件便确认，即返回
        return;
    ndata4.clear();
    QFile file(nfileName4);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "成功读取数据！", QMessageBox::Ok | QMessageBox::Default , 0 );

        while (!file.atEnd())
        {
          QByteArray line = file.readLine();
          QString str = QString(line);
          QStringList strlist=str.split("\n");  //按照空格分割
          double newdata = strlist[0].toDouble();
          ndata4<<newdata;
        }
        file.close();
        ui->txt4->setCheckState(Qt::Checked);
    }
}
void form4::on_clear4_clicked(){
    nfileName4 = "NULL";
    ndata4.clear();
    memset(nlable4, 0, sizeof (nlable4));       // 数组全部归零
    ui->txt4->setCheckState(Qt::Unchecked);
    ui->lable4->setCheckState(Qt::Unchecked);
}
void form4::on_label_button4_clicked(){
    if (ndata4.size() == 0){
        QMessageBox::warning(0 , "错误" , "请先选择数据", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    int a = 0;
    int b = 0;
    if(ui->comboBox4->currentIndex()==1) b = 1;
    else if(ui->comboBox4->currentIndex()==2) a = 1;
    else if(ui->comboBox4->currentIndex()==3) a = b = 1;
    int n = ndata4.size();
    int step = n / lablenum ;
    for(int i = 0; i < lablenum; i++){
        QList<double> alldata;
        for(int k = i*step; k <(i+1)*step; k++) alldata.append(ndata4[k]);
        //结果保留2位小数
        nlable4[i][0] = ((int)(Kurtosis_Value(alldata) * 100 + 0.5))/100.0;
        nlable4[i][1] = ((int)(Crest_Factor(alldata) * 100 + 0.5))/100.0;
        nlable4[i][2] = ((int)(Clearance_Factor(alldata) * 100 + 0.5))/100.0;
        nlable4[i][3] = ((int)(Impulse_Factor(alldata) * 100 + 0.5))/100.0;
        nlable4[i][4] = ((int)(Shape_Factor(alldata) * 100 + 0.5))/100.0;
        nlable4[i][5] = a;
        nlable4[i][6] = b;
    }
    ui->lable4->setCheckState(Qt::Checked);
}


// 保存标签到txt文件
void form4::on_savelabel_clicked(){

    prove();        //校验
    if(has < 2){
        QMessageBox::information(0 , "提示" , "保存失败，至少保存两组样本", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    if(simple == 0){
        QMessageBox::information(0 , "提示" , "保存失败，至少保存两组不同的样本", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }

    savefile = QFileDialog::getSaveFileName (this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (savefile.isEmpty())  return;   //如果未选择文件便确认，即返回
    //qDebug()<<savefile;
    QFile file(savefile);
    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            QTextStream stream(&file);
            stream.seek(file.size());

            if(nlable1[0][0] != 0){
                for(int i = 0; i < lablenum; i ++){
                    QString write;
                    write += QString::number(nlable1[i][0]) + " " + QString::number(nlable1[i][1]) + " " + QString::number(nlable1[i][2]) + " " + QString::number(nlable1[i][3]) + " " + QString::number(nlable1[i][4]) + " " + QString::number(nlable1[i][5]) + " " + QString::number(nlable1[i][6]);
                    stream << write << "\n";}}

            if(nlable2[0][0] != 0){
                for(int i = 0; i < lablenum; i ++){
                    QString write;
                    write += QString::number(nlable2[i][0]) + " " + QString::number(nlable2[i][1]) + " " + QString::number(nlable2[i][2]) + " " + QString::number(nlable2[i][3]) + " " + QString::number(nlable2[i][4]) + " " + QString::number(nlable2[i][5]) + " " + QString::number(nlable2[i][6]);
                    stream << write << "\n";}}

            if(nlable3[0][0] != 0){
                for(int i = 0; i < lablenum; i ++){
                    QString write;
                    write += QString::number(nlable3[i][0]) + " " + QString::number(nlable3[i][1]) + " " + QString::number(nlable3[i][2]) + " " + QString::number(nlable3[i][3]) + " " + QString::number(nlable3[i][4]) + " " + QString::number(nlable3[i][5]) + " " + QString::number(nlable3[i][6]);
                    stream << write << "\n";}}

            if(nlable4[0][0] != 0){
                for(int i = 0; i < lablenum; i ++){
                    QString write;
                    write += QString::number(nlable4[i][0]) + " " + QString::number(nlable4[i][1]) + " " + QString::number(nlable4[i][2]) + " " + QString::number(nlable4[i][3]) + " " + QString::number(nlable4[i][4]) + " " + QString::number(nlable4[i][5]) + " " + QString::number(nlable4[i][6]);
                    stream << write << "\n";}}
            file.close();
        }
}


void form4::on_readlabel_clicked(){

    labelin.clear();
    labelout.clear();
    alllabel.clear();

    trained = 0;
    readlable = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (readlable.isEmpty())    return;  //如果未选择文件便确认，即返回
    QFile file(readlable);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "已清除原数据，成功读取新数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        QTextStream in(&file);

        labelin.clear();
        labelout.clear();
        file.seek(0);
        while (!file.atEnd()){
            QByteArray line = file.readLine();
            QString str = QString(line).trimmed();
            QStringList strlist=str.split(" ");  //按照空格分割
            std::vector<double> newdatanall;
            for (int i = 0; i < 7; ++i){
               newdatanall.push_back(strlist[i].toDouble());
            }
            alllabel.push_back(newdatanall);
        }
        file.close();

        //打乱顺序，防止遗忘
        std::random_shuffle(alllabel.begin(), alllabel.end());
        int n = alllabel.size();
        for (int j = 0; j < n; ++j){
            std::vector<double> newdatanin;
            std::vector<double> newdatanout;
            for (int i = 0; i < 5; i++) {newdatanin.push_back(alllabel[j][i]);}
            for (int i = 5; i < 7; i++) {newdatanout.push_back(alllabel[j][i]);}
            labelin.push_back(newdatanin);
            labelout.push_back(newdatanout);
        }
    }
}



// 初始化bp网络
void form4::initdata(){
    //找到数据的最大最小值，为了做归一化处理
    int n = labelin.size();
    Minin.clear();
    Maxin.clear();
    for(int i = 0; i < 5; i ++){
        double a, b;
        a = b = labelin[0][i];
        for(int j = 0; j < n; j ++){
            a = a > labelin[j][i] ? a : labelin[j][i];
            b = b < labelin[j][i] ? b : labelin[j][i];
        }
        Maxin.push_back(a);
        Minin.push_back(b);
    }
    // 归一化处理
//    for (int i = 0; i < n; i++){
//        for (int j = 0; j < 5; j++){
//            labelin[i][j] = (labelin[i][j] - Minin[j]) / (Maxin[j] - Minin[j]);
//        }
//    }
    //初始化网络参数0-9 double
    for(int i = 0; i < In + 1; i++){
        for (int j = 0; j < Neuron; j ++){
            v[i][j] = rand() % 10 - 5;    //随机取-5 5的一个数
            dv[i][j] = 0;
        }
    }
    for(int i = 0; i < Neuron + 1; i++){
        for (int j = 0; j < Out; j ++){
            w[i][j] = rand() % 10 - 5;    //随机取0-9的一个数
            dw[i][j] = 0;
        }
    }
}

// 计算隐层和输出层的输出
void form4::output(int item){
    //item 第几个输入样本(第几行)
    //计算隐层输出
    for (int i = 0; i < Neuron; ++ i){
        double sum = 0;
        for (int j = 0; j < In;++ j) { sum += v[j][i] * labelin[item][j]; }
        sum += v[In][i] * -1;           //阈值-1
        o[i] = 1 / (1+exp(-1 * sum));   //激活函数
    }
    //隐藏层到输出层输出
    for (int i = 0; i < Out;++ i){
        double sum = 0;
        for (int j = 0; j < Neuron; ++ j) { sum += w[j][i] * o[j]; }
        sum += w[Neuron][i] * -1;       //阈值 永远是-1
        OutputData[i] = 1 / (1+exp(-1 * sum));
    }
}

// 反向传播 更新权值
void form4::backUpdate(int item){

    /*/ 计算输出层误差信号 /*/
    for(int k = 0; k < Out; ++k){
        delta_o[k] = (labelout[item][k] - OutputData[k]) * OutputData[k] * (1 - OutputData[k]);
    }

    /*/ 计算隐层误差信号 /*/
    for(int j = 0; j <Neuron; ++j){             //这里+1存疑
        double sum = 0;
        for(int k = 0; k < Out; ++k){
            sum += delta_o[k] * w[j][k];
        }
        delta_y[j] = sum * o[j] * (1 - o[j]);
    }

    /* 调整输入到隐层的权值 */
    for (int j = 0; j < Neuron; ++j){
        for (int k = 0; k < Out; ++k){
            w[j][k] += eta * delta_o[k] * o[j];
        }
        for (int k = 0; k < Out; ++k)  w[Neuron][k] += eta * delta_o[k] * -1;
    }
    /* 调整隐层到输出的权值 */
    for (int i = 0; i < In; ++i){
        for (int j = 0; j < Neuron; ++j)  v[i][j] += eta * delta_y[j] * labelin[item][i];
        for (int j = 0; j < Neuron; ++j)  v[In][j] += eta * delta_y[j] * -1;
    }
}

// 训练
void form4::trainBP(){
    int i,c = 0;
    double e = 0;
    edata.clear();
    initdata();
    int n = labelout.size();
    do{
        e = 0;              // 计算一次误差清零
        double wc = 0;
        for (i = 0; i < n; ++ i){
            output(i);
            for (int m = 0; m < Out; ++m)  wc += ((OutputData[m] - labelout[i][m]));        //计算一个样本的误差
            e += wc * wc;

            backUpdate(i);
        }
        e = sqrt(e);            // 计算全部样本的误差，单个样本误差的平方累加再开方
//        printf("item:%d, e:%f", c, e);
//        printf("\n");
        edata<<e;
        c ++;
    }while(c < TrainC);
    QMessageBox::information(0 , "提示" , "训练完成，迭代次数：" + QString::number(TrainC) , QMessageBox::Ok | QMessageBox::Default , 0 );
    trained = 1;

//    eCreatChart();
    eupdateChart();

}


void form4::on_trainBPnet_clicked(){
    if(labelin.size() == 0){
        QMessageBox::information(0 , "提示" , "错误，请先选择样本数据", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    trainBP();
}



void form4::on_saveModel_clicked(){
    prove();            //校验
    if(trained == 0){
        QMessageBox::information(0 , "提示" , "保存失败，请先训练模型", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    savefile = QFileDialog::getSaveFileName (this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (savefile.isEmpty())  return;   //如果未选择文件便确认，即返回
    QFile file(savefile);
    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            QTextStream stream(&file);
            stream.seek(file.size());
            //第一行保存模型包含的故障类型
            judgemodel();
            QString write;
            write += QString::number(modellable[0]) + " " + QString::number(modellable[1]) + " " + QString::number(modellable[2]) + " " + QString::number(modellable[3]);
            stream << write << "\n";
            //接下来保存v 输入到隐层的权值矩阵
            for (int i = 0; i < 6; i ++){
                QString writev;
                for (int j = 0; j < 9; j ++) {writev += QString::number(v[i][j]) + " ";}
                writev += QString::number(v[i][9]);
                stream << writev << "\n";
            }
            //最后保存w 隐层到输出层的权值矩阵
            for (int i = 0; i < 11; i ++){
                QString writew;
                writew += QString::number(w[i][0]) + " " + QString::number(w[i][1]);
                stream << writew << "\n";
            }

//            if(nlable1[0][0] != 0){
//                for(int i = 0; i < 60; i ++){
//                    QString write;
//                    write += QString::number(nlable1[i][0]) + " " + QString::number(nlable1[i][1]) + " " + QString::number(nlable1[i][2]) + " " + QString::number(nlable1[i][3]) + " " + QString::number(nlable1[i][4]) + " " + QString::number(nlable1[i][5]) + " " + QString::number(nlable1[i][6]);
//                    stream << write << "\n";}}
            file.close();
        }

}


// 判断模型中包含哪些故障类型
void form4::judgemodel(){
    memset(modellable, 0, sizeof (modellable));       // 数组全部归零
    int n = labelout.size();
    for(int i = 0; i < n; i++){
        if(labelout[i][0] == 0){
            if(labelout[i][1] == 0) modellable[0] = 1;
            else modellable[1] = 1;
        }else{
            if(labelout[i][1] == 0) modellable[2] = 1;
            else modellable[3] = 1;
        }
    }
}

// 验证制作标签的完整性
void form4::prove(){
    // 校验
    int a = 0;
    int b = 0;
    if(nlable1[0][0] != 0){
        has ++ ;
        if(has == 0){
            a = nlable1[0][5];
            b = nlable1[0][6];
        }else{
            if(a!=nlable1[0][5] || b!=nlable1[0][6]) simple = 1;
        }
    }
    if(nlable2[0][0] != 0){
        has ++ ;
        if(has == 0){
            a = nlable2[0][5];
            b = nlable2[0][6];
        }else{
            if(a!=nlable2[0][5] || b!=nlable2[0][6]) simple = 1;
        }
    }
    if(nlable3[0][0] != 0){
        has ++ ;
        if(has == 0){
            a = nlable3[0][5];
            b = nlable3[0][6];
        }else{
            if(a!=nlable3[0][5] || b!=nlable3[0][6]) simple = 1;
        }
    }
    if(nlable4[0][0] != 0){
        has ++ ;
        if(has == 0){
            a = nlable4[0][5];
            b = nlable4[0][6];
        }else{
            if(a!=nlable4[0][5] || b!=nlable4[0][6]) simple = 1;
        }
    }
}


// 读取待检测数据
void form4::on_readansdata_clicked(){

    predictname = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (predictname.isEmpty())  return;   //如果未选择文件便确认，即返回

    predict.clear();
    memset(predictdata, 0, sizeof (predictdata));
    QFile file(predictname);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QMessageBox::information(0 , "提示" , "成功读取数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        while (!file.atEnd())
        {
          QByteArray line = file.readLine();
          QString str = QString(line);
          QStringList strlist=str.split("\n");  //
          double newdata = strlist[0].toDouble();
          predict<<newdata;
        }
        file.close();
    }
    //计算时域特征参数
    predictdata[0] = ((int)(Kurtosis_Value(predict) * 100 + 0.5))/100.0;
    predictdata[1] = ((int)(Crest_Factor(predict) * 100 + 0.5))/100.0;
    predictdata[2] = ((int)(Clearance_Factor(predict) * 100 + 0.5))/100.0;
    predictdata[3] = ((int)(Impulse_Factor(predict) * 100 + 0.5))/100.0;
    predictdata[4] = ((int)(Shape_Factor(predict) * 100 + 0.5))/100.0;

    ui->qiaodu->clear();
    ui->qiaodu->append(QString::number(predictdata[0]));
    ui->fengzhi->clear();
    ui->fengzhi->append(QString::number(predictdata[1]));
    ui->yudu->clear();
    ui->yudu->append(QString::number(predictdata[2]));
    ui->maichong->clear();
    ui->maichong->append(QString::number(predictdata[3]));
    ui->boxing->clear();
    ui->boxing->append(QString::number(predictdata[4]));
}


// 加载模型按钮
void form4::on_loadmodel_clicked(){

    readmodelname = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (readmodelname.isEmpty())    return;  //如果未选择文件便确认，即返回
    QFile file(readmodelname);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);

        //初始化
        memset(w, 0, sizeof (w));
        memset(v, 0, sizeof (v));
        memset(modellable, 0, sizeof (modellable));
        file.seek(0);

        //先读第一行 模型label
        QByteArray line = file.readLine();
        QString str = QString(line).trimmed();
        QStringList strlist=str.split(" ");  //按照空格分割
        for (int i = 0; i < 4; i ++) modellable[i] = strlist[i].toInt();

        //读v 输入到隐层 6*10矩阵
        for (int i = 0; i < 6; i++){
            QByteArray line = file.readLine();
            QString str = QString(line).trimmed();
            QStringList strlist=str.split(" ");  //按照空格分割
            for (int j = 0; j < 10; j++) v[i][j] = strlist[j].toDouble();
        }

        //读w 隐层到输出 2*11矩阵
        for (int i = 0; i < 11; i++){
            QByteArray line = file.readLine();
            QString str = QString(line).trimmed();
            QStringList strlist=str.split(" ");  //按照空格分割
            for (int j = 0; j < 2; j++) w[i][j] = strlist[j].toDouble();
        }
        file.close();
        QString out;
        out += "此模型包含：";
        if (modellable[0] == 1) out += "正常数据，";
        if (modellable[1] == 1) out += "内圈损伤，";
        if (modellable[2] == 1) out += "外圈损伤，";
        if (modellable[3] == 1) out += "滚动体损伤，";
        out += "此模型只能预测以上故障类型！！";
         QMessageBox::information(0 , "注意" , out, QMessageBox::Ok | QMessageBox::Default , 0 );
    }
}


//预测按钮
void form4::on_analysis_clicked(){
//    int a, b;
    double yin[10];
    double chu[2];
    //计算隐层输出
    for (int i = 0; i < Neuron; ++ i){
        double sum = 0;
        for (int j = 0; j < In;++ j) { sum += v[j][i] * predictdata[j]; }
        sum += v[In][i] * -1;           //阈值-1
        yin[i] = 1 / (1+exp(-1 * sum));   //激活函数
    }
    //隐藏层到输出层输出
    for (int i = 0; i < Out;++ i){
        double sum = 0;
        for (int j = 0; j < Neuron; ++ j) { sum += w[j][i] * yin[j]; }
        sum += w[Neuron][i] * -1;       //阈值 永远是-1
        chu[i] = 1 / (1+exp(-1 * sum));
    }
    qDebug()<<chu[0]<<" "<<chu[1];
    int a, b;
    ui->guzhang->clear();
    a = round(chu[0]);
    b = round(chu[1]);
    qDebug()<<a<<" "<<b;
    if (a == 0){
        if (b == 0){
            ui->guzhang->append("正常轴承");
            QMessageBox::information(0 , "提示" , "正常轴承", QMessageBox::Ok | QMessageBox::Default , 0 );}
        else{
            QMessageBox::information(0 , "提示" , "内圈故障轴承", QMessageBox::Ok | QMessageBox::Default , 0 );
            ui->guzhang->append("内圈故障");}
    }else{
        if (b == 0 ) {
            ui->guzhang->append("外圈故障");
            QMessageBox::information(0 , "提示" , "外圈故障轴承", QMessageBox::Ok | QMessageBox::Default , 0 );
        }
        else {
            ui->guzhang->append("滚动体故障");
            QMessageBox::information(0 , "提示" , "滚动体故障轴承", QMessageBox::Ok | QMessageBox::Default , 0 );
        }
    }
}

// 设置训练次数
void form4::on_trainitems_clicked(){
    if(ui->traincombo->currentIndex()==0){
               QMessageBox::warning(0 , "设置训练次数" , "设置训练次数为50次", QMessageBox::Ok | QMessageBox::Default , 0 );
               TrainC = 50;
               }
    else if(ui->traincombo->currentIndex()==1) {
        QMessageBox::warning(0 , "设置训练次数" , "设置训练次数为100次", QMessageBox::Ok | QMessageBox::Default , 0 );
        TrainC = 100;
        }
    else if(ui->traincombo->currentIndex()==2) {
        QMessageBox::warning(0 , "设置训练次数" , "设置训练次数为200次", QMessageBox::Ok | QMessageBox::Default , 0 );
        TrainC = 200;
        }
    else if(ui->traincombo->currentIndex()==3) {
        QMessageBox::warning(0 , "设置训练次数" , "设置训练次数为500次", QMessageBox::Ok | QMessageBox::Default , 0 );
        TrainC = 500;
        }
    else if(ui->traincombo->currentIndex()==4) {
        QMessageBox::warning(0 , "设置训练次数" , "设置训练次数为1000次", QMessageBox::Ok | QMessageBox::Default , 0 );
        TrainC = 1000;
        }
    else if(ui->traincombo->currentIndex()==5) {
        QMessageBox::warning(0 , "设置训练次数" , "设置训练次数为2000次", QMessageBox::Ok | QMessageBox::Default , 0 );
        TrainC = 2000;
        }
    else if(ui->traincombo->currentIndex()==6) {
        QMessageBox::warning(0 , "设置训练次数" , "设置训练次数为5000次", QMessageBox::Ok | QMessageBox::Default , 0 );
        TrainC = 5000;
        }

}
double peak(QList<double> ndata){
    double ans = 0;
    double databs = 0;
    for (int i = 0; i < ndata.size(); i++){
        databs = qAbs(ndata[i]);
        if (databs > ans) ans = databs;
    }
    return ans;
}
double rmf(QList<double> ndata){
    double ans = 0;
    int n = ndata.size();
    for (int i = 0; i < n; i++) ans = ans + ndata[i] * ndata[i];
    return sqrt(ans / n);
}
double r(QList<double> ndata){
    double ans = 0;
    int n = ndata.size();
    for (int i = 0; i < n; i++) ans = ans + sqrt( abs(ndata[i]) );
    return (ans / n) * (ans / n);
}
double mean(QList<double> ndata){
    double ans = 0;
    int n = ndata.size();
    for (int i = 0; i < n; i++) ans = ans + abs(ndata[i]);
    return (ans / n);
}

double Kurtosis_Value(QList<double> ndata){
    double a = rmf(ndata);
    double ans = 0;
    int n = ndata.size();
    for (int i = 0; i < n; i++) ans = ans + ndata[i] *  ndata[i] *  ndata[i] *  ndata[i];
    return (ans / (n * a*a*a*a));
}
double Crest_Factor(QList<double> ndata){
    double a = peak(ndata);
    double b = rmf(ndata);
    return a / b;
}
double Clearance_Factor(QList<double> ndata){
    double a = peak(ndata);
    double b = r(ndata);
    return a / b;
}
double Impulse_Factor(QList<double> ndata){
    double a = peak(ndata);
    double b = mean(ndata);
    return a / b;
}
double Shape_Factor(QList<double> ndata){
    double a = rmf(ndata);
    double b = mean(ndata);
    return a / b;
}


// 创建图表
void form4::eCreatChart(){
    //QChart *chart = new QChart();
    chart = ebuildChart();
    ui->widget->setChart(chart);
    //ui->chartView1->setRenderHint(QPainter::Antialiasing);
}



// 设置图标属性，添加数据
QChart *form4::ebuildChart(){
    QChart *chart = new QChart();
    maxX = TrainC;
    maxY = 10;
    minY = 0;
    esplineSerie = new QLineSeries();
    //splineSeries1->setName("信号1");
    esplineSerie->setUseOpenGL(true);
    QPen pen1(0x000001);
    pen1.setWidth(3);
    //esplineSerie->setPen(pen1);              //设置画笔颜色和大小

    esplineSerie->append(0, 0);

////  添加数据1
//    esplineSerie->clear();
//    for (int i = 0; i < edata.size(); ++i){
//        esplineSerie->append(i, edata[i]);
//    }
    chart->addSeries(esplineSerie);         //添加数据源
    // 设置图表属性
    chart->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    chart->setTitle("训练均方误差曲线");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).back()->setRange(0, maxX);
    chart->axes(Qt::Horizontal).back()->setGridLineVisible();
    chart->axes(Qt::Horizontal).back()->setTitleText("训练轮数");
    chart->axes(Qt::Horizontal).back()->setLineVisible(true);
    chart->axes(Qt::Horizontal).back()->setLineVisible(true);
    chart->axes(Qt::Vertical).back()->setRange(minY, maxY);
    chart->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart->axes(Qt::Vertical).back()->setLineVisible(true);
    chart->axes(Qt::Vertical).back()->setTitleText("均方误差");
    return chart;
}


void form4::eupdateChart(){

    maxX = TrainC;
    maxY = edata[0];
    minY = 0;
    esplineSerie->clear();
    for (int i = 0; i < edata.size(); ++i){
        esplineSerie->append(i, edata[i]);
    }
    chart->addSeries(esplineSerie);         //添加数据源
    chart->setTitle("训练均方误差曲线");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).back()->setRange(0, maxX);
    chart->axes(Qt::Horizontal).back()->setGridLineVisible();
//    chart->axes(Qt::Horizontal).back()->setTitleText("训练轮数");
    chart->axes(Qt::Horizontal).back()->setLineVisible(true);
//    chart->axes(Qt::Horizontal).back()->hide();
    chart->axes(Qt::Vertical).back()->setRange(minY, maxY);
    chart->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart->axes(Qt::Vertical).back()->setLineVisible(true);
}

// 验证模型精度
void form4::on_verification_clicked(){

    if (trained == 0){
        QMessageBox::information(0 , "训练结果" , "您还没有训练，请先Train模型", QMessageBox::Ok | QMessageBox::Default , 0 );
        return;
    }
    int T = 0;
    int F = 0;
    int CF = 0;
    int LF = 0;
    int n = labelin.size();
    for (int k = 0; k < n; ++k){
        double yin[10];         //隐层输出
        double chu[2];          //输出层输出
        //计算隐层输出
        for (int i = 0; i < Neuron; ++ i){
            double sum = 0;
            for (int j = 0; j < In;++ j) { sum += v[j][i] * labelin[k][j]; }
            sum += v[In][i] * -1;           //阈值-1
            yin[i] = 1 / (1+exp(-1 * sum));   //激活函数
        }
        //隐藏层到输出层输出
        for (int i = 0; i < Out;++ i){
            double sum = 0;
            for (int j = 0; j < Neuron; ++ j) { sum += w[j][i] * yin[j]; }
            sum += w[Neuron][i] * -1;       //阈值 永远是-1
            chu[i] = 1 / (1+exp(-1 * sum));
        }
        int a = round(chu[0]);
        int b = round(chu[1]);
        if (a == labelout[k][0] && b == labelout[k][1]) T++;
        else {
            F++;
            if (a == 0 && b == 0) LF ++;        //漏检 把坏的识别成好的了
            else CF ++;                         //错检 识别错故障类型或把正常的识别成错误的
        }
//        qDebug()<<a<<" "<<b<<" "<<chu[0]<<" "<<chu[1]<<" "<<labelout[k][0]<<" "<<labelout[k][1];
    }
    QString varmessage;
    varmessage += "测试输入" + QString::number(n) + "个样本：\n" ;
    varmessage += "其中有" + QString::number(T) + "个样本预测准确；";
    varmessage += "有" + QString::number(F) + "个样本预测错误\n";
    varmessage += "预测错中有" +  QString::number(LF) + "个样本漏检；";
    varmessage += "有" +  QString::number(CF) + "个样本错检\n";
    varmessage += "怎么样，满意吗？\n如果精度较低您可以调整参数再次点击Train重新训练\n";
    varmessage += "如果满意，您可以点击保存模型将网络参数储存下来。";
    QMessageBox::information(0 , "训练结果" , varmessage, QMessageBox::Ok | QMessageBox::Default , 0 );
}






