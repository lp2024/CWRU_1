#include "form3.h"
#include "ui_form3.h"
#include <QUrl>
#include <QDesktopServices>

form3::form3(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form3){
    ui->setupUi(this);
}

form3::~form3()
{
    delete ui;
}


void form3::on_pushButton_clicked(){
    QDesktopServices::openUrl(QUrl(QLatin1String("https://gitee.com/li_hong_chen/CWRU")));
}



