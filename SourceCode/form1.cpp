#include "form1.h"
#include "ui_form1.h"
#include <QUrl>
#include <QDesktopServices>
#include <fstream>
#include <iostream>
using namespace std;

QString fileName1 = NULL;  //定义数据文件名
QString fileName2 = NULL;  //定义数据文件名
QList<double> data1;
QList<double> data2;


// 函数声明
double peak(QList<double> ndata);
double rmf(QList<double> ndata);
double r(QList<double> ndata);
double mean(QList<double> ndata);
double Kurtosis_Value(QList<double> ndata);
double Crest_Factor(QList<double> ndata);
double Clearance_Factor(QList<double> ndata);
double Impulse_Factor(QList<double> ndata);
double Shape_Factor(QList<double> ndata);


//构造函数
form1::form1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form1){
    ui->setupUi(this);
    //创建图表
    CreatChart();
}

// 析构函数
form1::~form1(){delete ui;}


// 创建图表
void form1::CreatChart(){
    //QChart *chart = new QChart();
    chart1 = buildChart1();
    chart2 = buildChart2();
    ui->chartView1->setChart(chart1);
    ui->chartView2->setChart(chart2);
    //ui->chartView1->setRenderHint(QPainter::Antialiasing);
}


// 设置图标属性，添加数据
QChart *form1::buildChart1(){
    QChart *chart = new QChart();
    maxX1 = 120000;
    tmax1 = 1;
    splineSeries1 = new QLineSeries();
    //splineSeries1->setName("信号1");
    splineSeries1->setUseOpenGL(true);
    QPen pen1(0x000001);
    pen1.setWidth(1);
    //splineSeries1->setPen(pen1);              //设置画笔颜色和大小
//  添加数据1
    splineSeries1->clear();
    chart->addSeries(splineSeries1);         //添加数据源
    // 设置图表属性
    chart->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).back()->setRange(0, maxX1);
    chart->axes(Qt::Horizontal).back()->setGridLineVisible(false);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    chart->axes(Qt::Horizontal).back()->hide();
    chart->axes(Qt::Vertical).back()->setRange(-tmax1, tmax1);
    chart->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
    return chart;
}

void form1::update1(){
    chart1->removeSeries(splineSeries1);
    splineSeries1->clear();
    QFile file(fileName1);
    int i = 0;
    data1.clear();
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while (!file.atEnd()){
            QByteArray line = file.readLine();
            QString str = QString(line);
            QStringList strlist=str.split("\n");  //按照空格分割
            double newdata = strlist[0].toDouble();
            qDebug()<<newdata;
            splineSeries1->append(i, newdata);
            data1<<newdata;
            i ++ ;
        }
        file.close();
        qDebug()<<"finish";
    }

    tmax1 = 0;
    maxX1 = data1.size();
    for (int n = 0; n < maxX1; ++n){
        if (data1[n] > tmax1) tmax1 = data1[n];
    }

    chart1->addSeries(splineSeries1);         //添加数据源
    chart1->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    chart1->createDefaultAxes();
    chart1->axes(Qt::Horizontal).back()->setRange(0, maxX1);
    chart1->axes(Qt::Horizontal).back()->setGridLineVisible(true);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    chart1->axes(Qt::Horizontal).back()->hide();
    chart1->axes(Qt::Vertical).back()->setRange(-1.05*tmax1, 1.05*tmax1);
    chart1->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart1->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart1->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
}


// 设置图标属性，添加数据
QChart *form1::buildChart2(){
    QChart *chart = new QChart();
    maxX2 = 120000;
    tmax2 = 1;
    splineSeries2 = new QLineSeries();
    //splineSeries1->setName("信号1");
    splineSeries2->setUseOpenGL(true);
    QPen pen1(0x000001);
    pen1.setWidth(1);
    //splineSeries1->setPen(pen1);              //设置画笔颜色和大小
//  添加数据
    splineSeries2->clear();
    chart->addSeries(splineSeries2);         //添加数据源
    // 设置图表属性
    chart->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).back()->setRange(0, maxX2);
    chart->axes(Qt::Horizontal).back()->setGridLineVisible(false);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    chart->axes(Qt::Horizontal).back()->hide();
    chart->axes(Qt::Vertical).back()->setRange(-tmax2, tmax2);
    chart->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
    return chart;
}


void form1::update2(){
    chart2->removeSeries(splineSeries2);
    splineSeries2->clear();
    QFile file(fileName2);
    int i = 0;
    data2.clear();
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while (!file.atEnd()){
            QByteArray line = file.readLine();
            QString str = QString(line);
            QStringList strlist=str.split("\n");  //按照空格分割
            double newdata = strlist[0].toDouble();
            qDebug()<<newdata;
            splineSeries2->append(i, newdata);
            data2<<newdata;
            i ++ ;
        }
        file.close();
        qDebug()<<"finish";
    }

    tmax2 = 0;
    maxX2 = data1.size();
    for (int n = 0; n < maxX2; ++n){
        if (data2[n] > tmax2) tmax2 = data2[n];
    }

    chart2->addSeries(splineSeries2);         //添加数据源
    chart2->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    chart2->createDefaultAxes();
    chart2->axes(Qt::Horizontal).back()->setRange(0, maxX2);
    chart2->axes(Qt::Horizontal).back()->setGridLineVisible(false);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    chart2->axes(Qt::Horizontal).back()->hide();
    chart2->axes(Qt::Vertical).back()->setRange(-1.05*tmax2, 1.05*tmax2);
    chart2->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart2->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart2->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
}



// 读取txt1(上面的图)
void form1::on_readfile1_clicked()
{
    fileName1 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (fileName1.isEmpty())     //如果未选择文件便确认，即返回
        return;
    QFile file(fileName1);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "成功读取数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        file.close();
    }

    update1();
    double feature[5];
    feature[0] = ((int)(Kurtosis_Value(data1) * 100 + 0.5))/100.0;
    feature[1] = ((int)(Crest_Factor(data1) * 100 + 0.5))/100.0;
    feature[2] = ((int)(Clearance_Factor(data1) * 100 + 0.5))/100.0;
    feature[3] = ((int)(Impulse_Factor(data1) * 100 + 0.5))/100.0;
    feature[4] = ((int)(Shape_Factor(data1) * 100 + 0.5))/100.0;



    ui->f1qiaodu1->clear();
    ui->f1qiaodu1->append(QString::number(feature[0]));
    ui->f1fengzhi1->clear();
    ui->f1fengzhi1->append(QString::number(feature[1]));
    ui->f1yudu1->clear();
    ui->f1yudu1->append(QString::number(feature[2]));
    ui->f1maichong1->clear();
    ui->f1maichong1->append(QString::number(feature[3]));
    ui->f1boxing1->clear();
    ui->f1boxing1->append(QString::number(feature[4]));
}


// 读取txt2(xia面的图)
void form1::on_readfile2_clicked()
{
    fileName2 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (fileName2.isEmpty())     //如果未选择文件便确认，即返回
        return;
    QFile file(fileName2);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "成功读取数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        file.close();
    }
    update2();
    double feature[5];
    feature[0] = ((int)(Kurtosis_Value(data2) * 100 + 0.5))/100.0;
    feature[1] = ((int)(Crest_Factor(data2) * 100 + 0.5))/100.0;
    feature[2] = ((int)(Clearance_Factor(data2) * 100 + 0.5))/100.0;
    feature[3] = ((int)(Impulse_Factor(data2) * 100 + 0.5))/100.0;
    feature[4] = ((int)(Shape_Factor(data2) * 100 + 0.5))/100.0;
    ui->f1qiaodu2->clear();
    ui->f1qiaodu2->append(QString::number(feature[0]));
    ui->f1fengzhi2->clear();
    ui->f1fengzhi2->append(QString::number(feature[1]));
    ui->f1yudu2->clear();
    ui->f1yudu2->append(QString::number(feature[2]));
    ui->f1maichong2->clear();
    ui->f1maichong2->append(QString::number(feature[3]));
    ui->f1boxing2->clear();
    ui->f1boxing2->append(QString::number(feature[4]));
}





