#ifndef FORM4_H
#define FORM4_H
#include <QWidget>
#include <QFileDialog> //文件选择
#include <QTextStream> //文件读写
#include <QtCharts/QChartGlobal>
#include <QtCharts/QChart>
#include <QMainWindow>
#include <QtCharts>
#include <QChartView>
#include <QSplineSeries>
#include <QScatterSeries>
#include<QVector>
#include <QTableWidgetItem>

#define In 5         //输入层神经元个数
#define Out 2        //输出层神经元个数
#define Neuron 10    //隐层神经元个数
#define labels 300   //样本数量

namespace Ui {
    class form4;
}

class form4 : public QWidget
{
    Q_OBJECT

public:
    explicit form4(QWidget *parent = 0);
    ~form4();

private slots:

    void on_txt_button1_clicked();
    void on_clear1_clicked();
    void on_label_button1_clicked();

    void on_txt_button2_clicked();
    void on_clear2_clicked();
    void on_label_button2_clicked();

    void on_txt_button3_clicked();
    void on_clear3_clicked();
    void on_label_button3_clicked();

    void on_txt_button4_clicked();
    void on_clear4_clicked();
    void on_label_button4_clicked();

    void on_savelabel_clicked();        //保存标签样本按钮
    void on_readlabel_clicked();        //读取标签样本按钮

    void on_verification_clicked();     //验证模型按钮

    void on_saveModel_clicked();        //保存模型按钮

    void on_trainBPnet_clicked();       //训练BP网络按钮

    void on_loadmodel_clicked();        //加载模型按钮
    void on_analysis_clicked();         //预测按钮
    void on_readansdata_clicked();      //加载待预测数据文件

    void on_trainitems_clicked();       //设置训练次数

private:
    void initdata();                //初始化BP网络
    void output(int i);             //计算隐层和输出层的输出
    void backUpdate(int i);         //反向传播
    void trainBP();                 //训练

    void judgemodel();              //判断模型包含哪几种故障类型
    void prove();                   //检验数据合理性

    void eCreatChart();
    QChart *ebuildChart();
    void eupdateChart();


private:

    //可变数组
    std::vector<std::vector<double>> alllabel;  //所有数据
    std::vector<std::vector<double>> labelin;   //时域参数
    std::vector<std::vector<double>> labelout;  //01标签
    std::vector<double> Maxin;      //归一化
    std::vector<double> Minin;      //归一化

    //文件参数
    QString nfileName1 = NULL;  //定义数据文件路径
    QString nfileName2 = NULL;  //定义数据文件路径
    QString nfileName3 = NULL;  //定义数据文件路径
    QString nfileName4 = NULL;  //定义数据文件路径
    QString savefile = NULL;
    QString readlable = NULL;
    QString readmodelname = NULL;   //定义模型文件据路径
    QString predictname = NULL;     //待预测数据文件路径

    //数据参数
    QList<double> ndata1;
    QList<double> ndata2;
    QList<double> ndata3;
    QList<double> ndata4;
    QList<double> predict;
    double modellable[4] = {0, 0, 0, 0};       //模型类别，用四个01二进制数来表示模型中是否包含故障
    //按照顺序分别是：正常数据，内圈，外圈，滚动体
    double predictdata[5];                     //待预测的时域特征数据

    //标签参数
    double nlable1[labels][7];      //标签1一共有50个样本
    double nlable2[labels][7];      //标签2一共有60个样本
    double nlable3[labels][7];      //标签3样本集合
    double nlable4[labels][7];      //标签4样本集合

    // bp网络参数
    double v[In + 1][Neuron];       //输入层到隐层的权值矩阵  /*这里的+1是激活阈值*/
    double w[Neuron + 1][Out];      //隐层到输出层的权值矩阵  /*这里的+1是激活阈值*/
    double dv[In + 1][Neuron];      //修正v
    double dw[Neuron + 1][Out];     //修正w
    double o[Neuron];               //记录的是神经元通过激活函数对外的输出
    double OutputData[Out];         //存储BP神经网络的输出
    double delta_o[Out];            //输出层误差信号
    double delta_y[Neuron];         //隐层误差信号
    double eta = 0.1;               //学习率
    int TrainC = 500;            //最大训练次数


    int trained = 0;                //标志位 1代表模型已经完成训练
    int has = 0;                    //标志位 是否有两组及以上的数据被选中
    int simple = 0;                 //标志位 是否有两种及以上的标签
    int lablenum = labels;             //样本数



private:
    Ui::form4 *ui;

    QChart *chart;                      //chart对象，绘图的载体
    QChartView *chartView;              //chartView,chart展示的载体
    int maxX;                           //X轴长度
    float maxY;                           //y轴最大值
    float minY;                           //y轴最小值
    QList<int> edata;                 //存储坐标数据，使用list可以很方便的增删
    QLineSeries *esplineSerie;        //绘图数据，连续曲线

};


#endif // FORM1_H
