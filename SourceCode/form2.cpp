#include "form2.h"
#include "ui_form2.h"
#include <QUrl>
#include <QDesktopServices>


form2::form2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form2){
    ui->setupUi(this);
    fCreatChart();

    ui->m_valueLabel->hide();//进行隐藏
    //鼠标移动到数据点上显示
    connect(fscatterSeries1, &QScatterSeries::hovered, this, &form2::slotPointHoverd);

}

form2::~form2(){delete ui;}

void form2::on_cvrd1_clicked(){
    fdata1.clear();
    fdataname1 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (fdataname1.isEmpty())    return;  //如果未选择文件便确认，即返回
    QFile file(fdataname1);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(0 , "提示" , "已清除原数据，成功读取新数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        QTextStream in(&file);

        file.seek(0);
        while (!file.atEnd()){
            QByteArray line = file.readLine();
            QString str = QString(line).trimmed();
//            QStringList strlist=line.split(" ");  //按照空格分割
            fdata1<<str.toDouble();
        }
        file.close();
    }
    fupdate1();
}


void form2::on_cvrd2_clicked(){
    FFTans1.clear();
    fdata2.clear();
    fdataname2 = QFileDialog::getOpenFileName(this,tr("选择txt文件"),"",tr("TXT(*.txt)")); //选择路径
    if (fdataname2.isEmpty())    return;  //如果未选择文件便确认，即返回
    QFile file(fdataname2);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QMessageBox::information(0 , "提示" , "已清除原数据，成功读取新数据！", QMessageBox::Ok | QMessageBox::Default , 0 );
        QTextStream in(&file);
        file.seek(0);
        while (!file.atEnd()){
            QByteArray line = file.readLine();
            QString str = QString(line).trimmed();
//            QStringList strlist=line.split(" ");  //按照空格分割
            fdata2<<str.toDouble();
        }
        file.close();
    }
    fupdate2();
}

void form2::FFTW1(){
//    int N = fdata1.size();
//    int N = 8192;
    //声明复数类型的输入数据in1_c和FFT变换的结果out1_c
    fftw_complex *in1_c, *out1_c;
    //声明变换策略
    fftw_plan p;
    //申请动态内存,这里构造二维数组的方式值得学习
    in1_c = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)* N);
    out1_c = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)* N);
    //返回变换策略
    p = fftw_plan_dft_1d(N, in1_c, out1_c, FFTW_FORWARD, FFTW_ESTIMATE);

    int n;
    for (n = 0; n<N; n++)//构造输入数据
    {
        in1_c[n][0] = fdata1[n];
        in1_c[n][1] = 0;
    }
    fftw_execute(p);//执行变换
    fftw_destroy_plan(p);//销毁策略

    FFTans1.clear();
    for (n = 0; n<N; n++){
        double ans = sqrt(pow(out1_c[n][0], 2) + pow(out1_c[n][1], 2));
        FFTans1 << ans;
        qDebug()<< ans;
    }
        //释放内存
    fftw_free(in1_c);
    fftw_free(out1_c);

    fmax1 = 0;
    vmax1.clear();
    int m = round(0.5 * FFTans1.size());
    for (int i = 0; i < m; ++i) {
        if (FFTans1[i] > fmax1){ fmax1 = FFTans1[i]; }
    }
    for (int i = 0; i < m; ++i){
        if (FFTans1[i] > 0.2 * fmax1) {
            std::vector<double> newin;
            newin.push_back(i);
            newin.push_back(FFTans1[i]);
            vmax1.push_back(newin);
        }
    }
}


void form2::FFTW2(){
    FFTans2.clear();
//    int N = fdata2.size();
    //声明复数类型的输入数据in1_c和FFT变换的结果out1_c
    fftw_complex *in1_c, *out1_c;
    //声明变换策略
    fftw_plan p;
    //申请动态内存,这里构造二维数组的方式值得学习
    in1_c = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)* N);
    out1_c = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)* N);
    //返回变换策略
    p = fftw_plan_dft_1d(N, in1_c, out1_c, FFTW_FORWARD, FFTW_ESTIMATE);

    int n;
    for (n = 0; n<N; n++)//构造输入数据
    {
        in1_c[n][0] = fdata2[n];
        in1_c[n][1] = 0;
    }
    fftw_execute(p);//执行变换
    fftw_destroy_plan(p);//销毁策略

    for (n = 0; n<N; n++){
        double ans = sqrt(pow(out1_c[n][0], 2) + pow(out1_c[n][1], 2));
        FFTans2 << ans;
        qDebug()<< ans;
    }
        //释放内存
        fftw_free(in1_c);
        fftw_free(out1_c);

    fmax2 = 0;
    int m = round(0.5 * FFTans2.size());
    for (int i = 0; i < m; ++i) {
        if (FFTans2[i] > fmax2){ fmax2 = FFTans2[i]; }
    }
}



void form2::fCreatChart(){
    fchart1 = fbuildChart1();
    fchart2 = fbuildChart2();
    ui->fchartView1->setChart(fchart1);
    ui->fchartView2->setChart(fchart2);
}


QChart *form2::fbuildChart1(){
    QChart *chart = new QChart();
    maxX = 120000;
    maxY = 1000;
    minY = 0;
    fsplineSeries1 = new QLineSeries();
    //splineSeries1->setName("信号1");
    fsplineSeries1->setUseOpenGL(true);
    QPen pen1(0x000001);
    pen1.setWidth(1);
    //splineSeries1->setPen(pen1);              //设置画笔颜色和大小
//  添加数据1
    fsplineSeries1->clear();
    chart->addSeries(fsplineSeries1);         //添加数据源


    fscatterSeries1 = new QScatterSeries();
    fscatterSeries1->setMarkerSize(10);
    chart->addSeries(fscatterSeries1);

    // 设置图表属性
    chart->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).back()->setRange(0, maxX);
    chart->axes(Qt::Horizontal).back()->setGridLineVisible(false);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    chart->axes(Qt::Horizontal).back()->hide();
    chart->axes(Qt::Vertical).back()->setRange(minY, maxY);
    chart->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
    return chart;
}


void form2::fupdate1(){

    FFTW1();

    fchart1->removeSeries(fsplineSeries1);
    fsplineSeries1->clear();


    fchart1->removeSeries(fscatterSeries1);
    fscatterSeries1->clear();
    for (int i = 0; i < vmax1.size(); ++i) {fscatterSeries1->append(vmax1[i][0], vmax1[i][1]); }
    fchart1->addSeries(fscatterSeries1);         //添加数据源


    int m = round(0.5 * FFTans1.size());
    maxX = m;
    maxY = 1.1*fmax1;
    minY = 0;
    for (int i = 0; i < m; ++i) {fsplineSeries1->append(i, FFTans1[i]);}

    fchart1->addSeries(fsplineSeries1);         //添加数据源
    fchart1->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    fchart1->createDefaultAxes();
    fchart1->axes(Qt::Horizontal).back()->setRange(0, maxX);
    fchart1->axes(Qt::Horizontal).back()->setGridLineVisible(false);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    fchart1->axes(Qt::Horizontal).back()->hide();
    fchart1->axes(Qt::Vertical).back()->setRange(minY, maxY);
    fchart1->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    fchart1->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    fchart1->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
}




QChart *form2::fbuildChart2(){
    QChart *chart = new QChart();
    maxX = 120000;
    maxY = 1000;
    minY = 0;
    fsplineSeries2 = new QLineSeries();
    //splineSeries1->setName("信号1");
    fsplineSeries2->setUseOpenGL(true);
    QPen pen1(0x000001);
    pen1.setWidth(1);
    //splineSeries1->setPen(pen1);              //设置画笔颜色和大小
//  添加数据
    fsplineSeries2->clear();
    chart->addSeries(fsplineSeries2);         //添加数据源
    // 设置图表属性
    chart->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).back()->setRange(0, maxX);
    chart->axes(Qt::Horizontal).back()->setGridLineVisible(false);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    chart->axes(Qt::Horizontal).back()->hide();
    chart->axes(Qt::Vertical).back()->setRange(minY, maxY);
    chart->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    chart->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    chart->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
    return chart;
}



void form2::fupdate2(){
    fchart2->removeSeries(fsplineSeries2);
    fsplineSeries2->clear();

    FFTW2();
    maxX = FFTans2.size();
    maxY = 1.1*fmax2;
    minY = 0;

    for (int i = 0; i < FFTans2.size(); ++i) {fsplineSeries2->append(i, FFTans2[i]);}

    fchart2->addSeries(fsplineSeries2);         //添加数据源
    fchart2->legend()->setVisible(false);     //如果需要打开legned 改为true
    //chart->legend()->setAlignment(Qt::AlignRight);
    //chart->setTitle("红尘出品");
    fchart2->createDefaultAxes();
    fchart2->axes(Qt::Horizontal).back()->setRange(0, maxX);
    fchart2->axes(Qt::Horizontal).back()->setGridLineVisible(false);
    //chart->axes(Qt::Horizontal).back()->setTitleText("X轴");
    fchart2->axes(Qt::Horizontal).back()->hide();
    fchart2->axes(Qt::Vertical).back()->setRange(minY, maxY);
    fchart2->axes(Qt::Vertical).back()->setGridLineVisible(true);  //主网格线
    fchart2->axes(Qt::Vertical).back()->setMinorGridLineVisible(true);  //次网格线
    fchart2->axes(Qt::Vertical).back()->setLineVisible(true);
    //chart->axes(Qt::Vertical).back()->setTitleText("Y轴");
}




void form2::slotPointHoverd(const QPointF &point, bool state){
    if (state) {
            ui->m_valueLabel->setText(QString::asprintf("%.1f Hz", point.x()));

            QPoint curPos = mapFromGlobal(QCursor::pos());
            ui->m_valueLabel->move(curPos.x() + ui->m_valueLabel->width() / 5, curPos.y() - ui->m_valueLabel->height() * 1.5);//移动数值

            ui->m_valueLabel->show();//显示出来
        }
        else
            ui->m_valueLabel->hide();//进行隐藏
}



void form2::on_redraw_clicked(){
    if(ui->comboBox->currentIndex() == 0) N = 1024;
    else if(ui->comboBox->currentIndex() == 1) N = 2048;
    else if(ui->comboBox->currentIndex() == 2) N = 4096;
    else if(ui->comboBox->currentIndex() == 3) N = 6144;
    else if(ui->comboBox->currentIndex() == 4) N = 8192;
    else if(ui->comboBox->currentIndex() == 5) N = 10240;

    if (fdata1.size() != 0 ) fupdate1();
    if (fdata2.size() != 0 ) fupdate2();
}




