QT       += core gui axcontainer charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    form1.cpp \
    form2.cpp \
    form3.cpp \
    form4.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    fftw3.h \
    form1.h \
    form2.h \
    form3.h \
    form4.h \
    mainwindow.h

FORMS += \
    mainwindow.ui \
    ui/form1.ui \
    ui/form2.ui \
    ui/form3.ui \
    ui/form4.ui \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#tianjia
LIBS += F:\Code_2\qt_projection\Stacked\Stacked\libfftw3-3.lib

