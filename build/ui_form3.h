/********************************************************************************
** Form generated from reading UI file 'form3.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM3_H
#define UI_FORM3_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_form3
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton;
    QTextBrowser *textBrowser;

    void setupUi(QWidget *form3)
    {
        if (form3->objectName().isEmpty())
            form3->setObjectName(QString::fromUtf8("form3"));
        form3->resize(1000, 600);
        verticalLayoutWidget = new QWidget(form3);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(810, 530, 191, 71));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font;
        font.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font.setPointSize(11);
        pushButton->setFont(font);
        pushButton->setStyleSheet(QString::fromUtf8("border-image: url(:/photo/whiteAll.png);"));

        verticalLayout->addWidget(pushButton);

        textBrowser = new QTextBrowser(form3);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setEnabled(true);
        textBrowser->setGeometry(QRect(20, 20, 971, 491));

        retranslateUi(form3);

        QMetaObject::connectSlotsByName(form3);
    } // setupUi

    void retranslateUi(QWidget *form3)
    {
        form3->setWindowTitle(QCoreApplication::translate("form3", "Form", nullptr));
        pushButton->setText(QCoreApplication::translate("form3", "\347\202\271\345\207\273\346\237\245\347\234\213\346\233\264\345\244\232\342\200\246\342\200\246", nullptr));
        textBrowser->setHtml(QCoreApplication::translate("form3", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt; font-weight:600;\">\344\275\277\347\224\250\350\257\264\346\230\216\357\274\232</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">- \346\234\254\347\263\273\347\273\237\345\256\236\347\216\260\344\272\206\346\227\266\345\237\237\345\210\206\346\236\220\343\200\201\351\242\221\345\237\237\345\210\206\346\236\220\343\200\201\345\237\272\344\272\216BP\347\245\236\347\273\217\347\275\221\347\273\234\347"
                        "\232\204\346\225\205\351\232\234\350\257\212\346\226\255\344\270\211\344\270\252\345\212\237\350\203\275\357\274\214\346\202\250\345\217\257\344\273\245\347\202\271\345\207\273\344\270\273\347\225\214\351\235\242\344\270\212\346\226\271\346\214\211\351\222\256\345\210\207\346\215\242\345\212\237\350\203\275\351\241\265\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">- \346\227\266\345\237\237\345\210\206\346\236\220\357\274\232</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273\350\257\273\345\217\226\346\226\207\344\273\266\346\214\211\351\222\256\357\274\214\351\200\211\346\213\251\346\225\260\346"
                        "\215\256\346\226\207\346\234\254\343\200\202\347\273\230\345\233\276\351\200\237\345\272\246\345\217\227\346\225\260\346\215\256\351\207\217\345\275\261\345\223\215\357\274\214\346\225\260\346\215\256\351\207\217\350\276\203\345\244\247\350\257\267\347\250\215\347\255\211\347\211\207\345\210\273\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \345\217\263\344\276\247\346\230\276\347\244\272\346\227\266\345\237\237\347\211\271\345\276\201\345\217\202\346\225\260\343\200\202</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">- \351\242\221\345\237\237\345\210\206\346\236\220\357\274\232</span></p>\n"
"<p style=\" margin-top"
                        ":12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273\351\242\221\345\237\237\345\210\206\346\236\220\346\214\211\351\222\256\357\274\214\350\277\233\345\205\245\351\242\221\345\237\237\345\210\206\346\236\220\347\225\214\351\235\242</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273\350\257\273\345\217\226\346\226\207\344\273\266\357\274\214\351\200\211\346\213\251\346\225\260\346\215\256\346\226\207\344\273\266</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \345\257\271\345\272\224\347\224\273\345\233\276\346\241\206\347\273\230\345\210\266\347\246\273\346\225\243\345\202\205\351\207\214\345\217\266\345\217\230\346\215\242\345\271\205"
                        "\345\200\274\350\260\261\347\272\277</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \345\217\257\344\273\245\346\233\264\346\224\271N\357\274\214\347\204\266\345\220\216\347\202\271\345\207\273\351\207\215\346\226\260\347\273\230\345\233\276\346\214\211\351\222\256\357\274\214\351\207\215\346\226\260\347\273\230\345\210\266\345\271\205\345\200\274\350\260\261</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \344\270\212\351\235\242\347\232\204\347\273\230\345\233\276\346\241\206\347\273\230\345\210\266\345\271\205\345\200\274\350\260\261\347\232\204\344\270\200\345\215\212\357\274\210\345\233\240\344\270\272\346\230\257\345\257\271\347\247\260\347\232\204\357\274\211\357\274\214\344\270\213\346\226\271\344\274\232\345\233\276\346\241\206\347\273\230\345\210\266\345"
                        "\205\250\351\203\250\345\271\205\345\200\274\350\260\261</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \351\274\240\346\240\207\347\247\273\345\212\250\350\207\263\344\270\212\346\226\271\345\271\205\345\200\274\350\260\261\347\232\204\345\263\260\345\200\274\347\202\271\344\270\212\357\274\214\345\217\257\344\273\245\346\230\276\347\244\272\345\257\271\345\272\224\347\232\204\351\242\221\347\216\207</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">- BP\347\245\236\347\273\217\347\275\221\347\273\234\346\225\205\351\232\234\351\242\204\346\265\213-\344\271\213-\350\256\255\347\273\203\347\275"
                        "\221\347\273\234</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273\346\225\205\351\232\234\351\242\204\346\265\213\346\214\211\351\222\256\357\274\214\350\277\233\345\205\245\346\225\205\351\232\234\351\242\204\346\265\213\347\225\214\351\235\242</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \346\234\200\345\244\232\346\224\257\346\214\2014\347\247\215\346\225\205\351\232\234\347\261\273\345\236\213\346\243\200\346\265\213\357\274\232\346\255\243\345\270\270\343\200\201\345\206\205\345\234\210\346\225\205\351\232\234\343\200\201\345\244\226\345\234\210\346\225\205\351\232\234\343\200\201\346\273\232\345\212\250\344\275\223\346\225\205\351\232\234</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0"
                        "px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \345\210\266\344\275\234\346\250\241\345\236\213\345\217\257\344\273\245\345\210\266\344\275\234\345\214\205\345\220\253\344\270\244\347\247\215\345\217\212\344\273\245\344\270\212\347\232\204\346\225\205\351\232\234\351\242\204\346\265\213\346\250\241\345\236\213</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \351\200\211\346\213\251\345\212\240\350\275\275txt\357\274\214\345\212\240\350\275\275\346\225\260\346\215\256\357\274\214\345\212\240\350\275\275\345\256\214\346\210\220\357\274\214CheckBox\344\274\232\345\217\230\344\270\272Check\347\212\266\346\200\201</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\204\266\345\220\216\344\273\216ComboBox\344\270\255\351\200\211\346\213\251"
                        "\346\225\205\351\232\234\347\261\273\345\236\213\357\274\214\347\202\271\345\207\273\345\210\266\344\275\234\346\240\207\347\255\276\357\274\214\345\210\266\344\275\234\345\256\214\346\210\220\357\274\214CheckBox\344\274\232\345\217\230\344\270\272Check\347\212\266\346\200\201</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \345\246\202\346\236\234\346\237\220\344\270\252\346\225\260\346\215\256\351\224\231\350\257\257\357\274\214\345\217\257\344\273\245\347\202\271\345\207\273\345\217\263\344\276\247\347\232\204\346\270\205\351\231\244\346\225\260\346\215\256\346\214\211\351\222\256\357\274\214\346\270\205\351\231\244\346\254\241\350\241\214\346\225\260\346\215\256\357\274\214\345\257\271\345\272\224\347\232\204CheckBox\344\271\237\344\274\232\345\217\230\346\210\220UnCheck\347\212\266\346\200\201</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right"
                        ":0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \345\210\266\344\275\234\345\256\214\346\210\220\345\220\216\347\202\271\345\207\273\344\277\235\345\255\230\346\240\207\347\255\276\357\274\214\344\277\235\345\255\230\344\270\272txt\346\226\207\344\273\266\357\274\214\346\226\207\344\273\266\345\206\205\345\256\271\345\246\202\344\270\213\357\274\232\346\257\217\344\270\200\345\210\227\344\273\243\350\241\250\344\270\200\344\270\252\346\240\267\346\234\254\357\274\214\346\257\217\344\270\200\350\241\214\347\232\204\345\211\2155\344\270\252\344\270\272\346\227\266\345\237\237\347\211\271\345\276\201\345\217\202\346\225\260\357\274\214\345\220\2162\344\270\252\344\270\27201\345\217\230\351\207\217\357\274\214\347\224\25000\343\200\20101\343\200\20110\343\200\20111\344\273\243\350\241\250\346\234\200\345\244\232\345\233\233\347\247\215\346\225\205\351\232\234\347\261\273\345\236\213</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt"
                        "-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\204\266\345\220\216\347\202\271\345\207\273\350\257\273\345\217\226\346\240\267\346\234\254\346\214\211\351\222\256\357\274\214\350\257\273\345\217\226\345\267\262\347\273\217\345\210\266\344\275\234\345\245\275\347\232\204\346\240\267\346\234\254\346\226\207\344\273\266</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \351\273\230\350\256\244\350\256\255\347\273\203\350\275\256\346\254\241\344\270\272500\350\275\256\357\274\214\345\217\257\344\273\245\347\202\271\345\207\273\346\233\264\346\224\271\350\256\255\347\273\203\346\254\241\346\225\260\346\233\264\346\224\271\350\256\255\347\273\203\350\275\256\346\225\260</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273Trai"
                        "n\346\214\211\351\222\256\357\274\214\350\277\233\350\241\214\350\256\255\347\273\203</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \350\256\255\347\273\203\347\273\223\346\235\237\357\274\214\347\202\271\345\207\273\351\252\214\350\257\201\346\250\241\345\236\213\357\274\214\350\257\204\344\274\260\346\250\241\345\236\213\357\274\214\345\205\266\344\270\255\346\274\217\346\243\200\346\240\267\346\234\254\345\220\253\344\271\211\346\230\257\345\260\206\345\216\237\346\234\254\346\225\205\351\232\234\347\232\204\346\240\267\346\234\254\351\242\204\346\265\213\344\270\272\346\255\243\345\270\270\346\240\267\346\234\254\357\274\214\351\224\231\346\243\200\346\240\267\346\234\254\345\220\253\344\271\211\346\230\257\345\260\206\346\255\243\345\270\270\346\240\267\346\234\254\351\242\204\346\265\213\344\270\272\346\225\205\351\232\234\346\240\267\346\234\254\343\200\201\346\210\226\351\242\204"
                        "\346\265\213\351\224\231\350\257\257\346\225\205\351\232\234\347\261\273\345\236\213</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \345\257\271\347\273\223\346\236\234\344\270\215\346\273\241\346\204\217\345\217\257\344\273\245\351\207\215\345\244\215\344\270\212\344\270\244\344\270\252\346\255\245\351\252\244\357\274\214\346\273\241\346\204\217\347\232\204\350\257\235\347\202\271\345\207\273\344\277\235\345\255\230\346\250\241\345\236\213\357\274\214\345\202\250\345\255\230BP\347\275\221\347\273\234\345\217\202\346\225\260</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p>\n"
""
                        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">- BP\347\245\236\347\273\217\347\275\221\347\273\234\346\225\205\351\232\234\351\242\204\346\265\213-\344\271\213-\346\225\205\351\232\234\351\242\204\346\265\213</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273\345\212\240\350\275\275\347\275\221\347\273\234\357\274\214\351\200\211\346\213\251\344\277\235\345\255\230\347\232\204\346\250\241\345\236\213\357\274\214\346\255\244\346\227\266\344\274\232\346\217\220\347\244\272\346\250\241\345\236\213\345\217\257\344\273\245\351\242\204\346\265\213\347\232\204\346\225\205\351\232\234\347\247\215\347\261\273\357\274\214\344\270\215\345\234\250\350\214\203\345\233\264\345\206\205\347\232\204\346\225\205\351\232\234\346\227\240\346\263\225\345\207\206\347\241\256\351\242"
                        "\204\346\265\213</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273\350\257\273\345\217\226\346\225\260\346\215\256\357\274\214\351\200\211\346\213\251\345\276\205\346\243\200\346\265\213\347\232\204\350\275\264\346\211\277\346\225\260\346\215\256</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">	- \347\202\271\345\207\273\345\210\206\346\236\220\351\242\204\346\265\213\357\274\214\346\217\220\347\244\272\346\225\205\351\232\234\347\261\273\345\236\213</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class form3: public Ui_form3 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM3_H
