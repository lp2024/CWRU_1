/********************************************************************************
** Form generated from reading UI file 'form4.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM4_H
#define UI_FORM4_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qchartview.h"

QT_BEGIN_NAMESPACE

class Ui_form4
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QPushButton *txt_button1;
    QCheckBox *txt1;
    QComboBox *comboBox1;
    QPushButton *label_button1;
    QCheckBox *lable1;
    QPushButton *clear1;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QPushButton *txt_button2;
    QCheckBox *txt2;
    QComboBox *comboBox2;
    QPushButton *label_button2;
    QCheckBox *lable2;
    QPushButton *clear2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QPushButton *txt_button3;
    QCheckBox *txt3;
    QComboBox *comboBox3;
    QPushButton *label_button3;
    QCheckBox *lable3;
    QPushButton *clear3;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_4;
    QPushButton *txt_button4;
    QCheckBox *txt4;
    QComboBox *comboBox4;
    QPushButton *label_button4;
    QCheckBox *lable4;
    QPushButton *clear4;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *savelabel;
    QPushButton *readlabel;
    QPushButton *trainBPnet;
    QPushButton *verification;
    QPushButton *saveModel;
    QLabel *kuang1;
    QLabel *label_5;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QTextBrowser *qiaodu;
    QTextBrowser *fengzhi;
    QTextBrowser *yudu;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *readansdata;
    QPushButton *loadmodel;
    QPushButton *analysis;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *verticalLayout_6;
    QTextBrowser *maichong;
    QTextBrowser *boxing;
    QTextBrowser *guzhang;
    QLabel *label_12;
    QLabel *label_13;
    QChartView *widget;
    QPushButton *trainitems;
    QComboBox *traincombo;

    void setupUi(QWidget *form4)
    {
        if (form4->objectName().isEmpty())
            form4->setObjectName(QString::fromUtf8("form4"));
        form4->resize(1024, 600);
        verticalLayoutWidget = new QWidget(form4);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(50, 40, 606, 201));
        QFont font;
        font.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font.setPointSize(11);
        verticalLayoutWidget->setFont(font);
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        horizontalLayout->addWidget(label);

        txt_button1 = new QPushButton(verticalLayoutWidget);
        txt_button1->setObjectName(QString::fromUtf8("txt_button1"));
        txt_button1->setFont(font);

        horizontalLayout->addWidget(txt_button1);

        txt1 = new QCheckBox(verticalLayoutWidget);
        txt1->setObjectName(QString::fromUtf8("txt1"));
        txt1->setEnabled(false);
        txt1->setFont(font);

        horizontalLayout->addWidget(txt1);

        comboBox1 = new QComboBox(verticalLayoutWidget);
        comboBox1->addItem(QString());
        comboBox1->addItem(QString());
        comboBox1->addItem(QString());
        comboBox1->addItem(QString());
        comboBox1->setObjectName(QString::fromUtf8("comboBox1"));
        comboBox1->setFont(font);

        horizontalLayout->addWidget(comboBox1);

        label_button1 = new QPushButton(verticalLayoutWidget);
        label_button1->setObjectName(QString::fromUtf8("label_button1"));
        label_button1->setFont(font);

        horizontalLayout->addWidget(label_button1);

        lable1 = new QCheckBox(verticalLayoutWidget);
        lable1->setObjectName(QString::fromUtf8("lable1"));
        lable1->setEnabled(false);
        lable1->setFont(font);

        horizontalLayout->addWidget(lable1);

        clear1 = new QPushButton(verticalLayoutWidget);
        clear1->setObjectName(QString::fromUtf8("clear1"));

        horizontalLayout->addWidget(clear1);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        horizontalLayout_2->addWidget(label_2);

        txt_button2 = new QPushButton(verticalLayoutWidget);
        txt_button2->setObjectName(QString::fromUtf8("txt_button2"));
        txt_button2->setFont(font);

        horizontalLayout_2->addWidget(txt_button2);

        txt2 = new QCheckBox(verticalLayoutWidget);
        txt2->setObjectName(QString::fromUtf8("txt2"));
        txt2->setEnabled(false);
        txt2->setFont(font);

        horizontalLayout_2->addWidget(txt2);

        comboBox2 = new QComboBox(verticalLayoutWidget);
        comboBox2->addItem(QString());
        comboBox2->addItem(QString());
        comboBox2->addItem(QString());
        comboBox2->addItem(QString());
        comboBox2->setObjectName(QString::fromUtf8("comboBox2"));
        comboBox2->setFont(font);

        horizontalLayout_2->addWidget(comboBox2);

        label_button2 = new QPushButton(verticalLayoutWidget);
        label_button2->setObjectName(QString::fromUtf8("label_button2"));
        label_button2->setFont(font);

        horizontalLayout_2->addWidget(label_button2);

        lable2 = new QCheckBox(verticalLayoutWidget);
        lable2->setObjectName(QString::fromUtf8("lable2"));
        lable2->setEnabled(false);
        lable2->setFont(font);

        horizontalLayout_2->addWidget(lable2);

        clear2 = new QPushButton(verticalLayoutWidget);
        clear2->setObjectName(QString::fromUtf8("clear2"));

        horizontalLayout_2->addWidget(clear2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        horizontalLayout_4->addWidget(label_3);

        txt_button3 = new QPushButton(verticalLayoutWidget);
        txt_button3->setObjectName(QString::fromUtf8("txt_button3"));
        txt_button3->setFont(font);

        horizontalLayout_4->addWidget(txt_button3);

        txt3 = new QCheckBox(verticalLayoutWidget);
        txt3->setObjectName(QString::fromUtf8("txt3"));
        txt3->setEnabled(false);
        txt3->setFont(font);

        horizontalLayout_4->addWidget(txt3);

        comboBox3 = new QComboBox(verticalLayoutWidget);
        comboBox3->addItem(QString());
        comboBox3->addItem(QString());
        comboBox3->addItem(QString());
        comboBox3->addItem(QString());
        comboBox3->setObjectName(QString::fromUtf8("comboBox3"));
        comboBox3->setFont(font);

        horizontalLayout_4->addWidget(comboBox3);

        label_button3 = new QPushButton(verticalLayoutWidget);
        label_button3->setObjectName(QString::fromUtf8("label_button3"));
        label_button3->setFont(font);

        horizontalLayout_4->addWidget(label_button3);

        lable3 = new QCheckBox(verticalLayoutWidget);
        lable3->setObjectName(QString::fromUtf8("lable3"));
        lable3->setEnabled(false);
        lable3->setFont(font);

        horizontalLayout_4->addWidget(lable3);

        clear3 = new QPushButton(verticalLayoutWidget);
        clear3->setObjectName(QString::fromUtf8("clear3"));

        horizontalLayout_4->addWidget(clear3);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_4 = new QLabel(verticalLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        horizontalLayout_5->addWidget(label_4);

        txt_button4 = new QPushButton(verticalLayoutWidget);
        txt_button4->setObjectName(QString::fromUtf8("txt_button4"));
        txt_button4->setFont(font);

        horizontalLayout_5->addWidget(txt_button4);

        txt4 = new QCheckBox(verticalLayoutWidget);
        txt4->setObjectName(QString::fromUtf8("txt4"));
        txt4->setEnabled(false);
        txt4->setFont(font);

        horizontalLayout_5->addWidget(txt4);

        comboBox4 = new QComboBox(verticalLayoutWidget);
        comboBox4->addItem(QString());
        comboBox4->addItem(QString());
        comboBox4->addItem(QString());
        comboBox4->addItem(QString());
        comboBox4->setObjectName(QString::fromUtf8("comboBox4"));
        comboBox4->setFont(font);

        horizontalLayout_5->addWidget(comboBox4);

        label_button4 = new QPushButton(verticalLayoutWidget);
        label_button4->setObjectName(QString::fromUtf8("label_button4"));
        label_button4->setFont(font);

        horizontalLayout_5->addWidget(label_button4);

        lable4 = new QCheckBox(verticalLayoutWidget);
        lable4->setObjectName(QString::fromUtf8("lable4"));
        lable4->setEnabled(false);
        lable4->setFont(font);

        horizontalLayout_5->addWidget(lable4);

        clear4 = new QPushButton(verticalLayoutWidget);
        clear4->setObjectName(QString::fromUtf8("clear4"));

        horizontalLayout_5->addWidget(clear4);


        verticalLayout->addLayout(horizontalLayout_5);

        verticalLayoutWidget_2 = new QWidget(form4);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(670, 50, 111, 181));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        savelabel = new QPushButton(verticalLayoutWidget_2);
        savelabel->setObjectName(QString::fromUtf8("savelabel"));

        verticalLayout_2->addWidget(savelabel);

        readlabel = new QPushButton(verticalLayoutWidget_2);
        readlabel->setObjectName(QString::fromUtf8("readlabel"));

        verticalLayout_2->addWidget(readlabel);

        trainBPnet = new QPushButton(verticalLayoutWidget_2);
        trainBPnet->setObjectName(QString::fromUtf8("trainBPnet"));

        verticalLayout_2->addWidget(trainBPnet);

        verification = new QPushButton(verticalLayoutWidget_2);
        verification->setObjectName(QString::fromUtf8("verification"));

        verticalLayout_2->addWidget(verification);

        saveModel = new QPushButton(verticalLayoutWidget_2);
        saveModel->setObjectName(QString::fromUtf8("saveModel"));

        verticalLayout_2->addWidget(saveModel);

        kuang1 = new QLabel(form4);
        kuang1->setObjectName(QString::fromUtf8("kuang1"));
        kuang1->setGeometry(QRect(0, 40, 791, 201));
        kuang1->setFrameShape(QFrame::Box);
        label_5 = new QLabel(form4);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 60, 31, 141));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font1.setPointSize(12);
        label_5->setFont(font1);
        label_5->setFrameShape(QFrame::Box);
        verticalLayoutWidget_3 = new QWidget(form4);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(160, 430, 141, 131));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        qiaodu = new QTextBrowser(verticalLayoutWidget_3);
        qiaodu->setObjectName(QString::fromUtf8("qiaodu"));
        qiaodu->setEnabled(false);

        verticalLayout_3->addWidget(qiaodu);

        fengzhi = new QTextBrowser(verticalLayoutWidget_3);
        fengzhi->setObjectName(QString::fromUtf8("fengzhi"));
        fengzhi->setEnabled(false);

        verticalLayout_3->addWidget(fengzhi);

        yudu = new QTextBrowser(verticalLayoutWidget_3);
        yudu->setObjectName(QString::fromUtf8("yudu"));
        yudu->setEnabled(false);

        verticalLayout_3->addWidget(yudu);

        verticalLayoutWidget_4 = new QWidget(form4);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(50, 430, 102, 131));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(verticalLayoutWidget_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font2.setPointSize(12);
        label_6->setFont(font2);

        verticalLayout_4->addWidget(label_6);

        label_7 = new QLabel(verticalLayoutWidget_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font2);

        verticalLayout_4->addWidget(label_7);

        label_8 = new QLabel(verticalLayoutWidget_4);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font2);

        verticalLayout_4->addWidget(label_8);

        horizontalLayoutWidget = new QWidget(form4);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(50, 360, 293, 51));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        readansdata = new QPushButton(horizontalLayoutWidget);
        readansdata->setObjectName(QString::fromUtf8("readansdata"));

        horizontalLayout_3->addWidget(readansdata);

        loadmodel = new QPushButton(horizontalLayoutWidget);
        loadmodel->setObjectName(QString::fromUtf8("loadmodel"));

        horizontalLayout_3->addWidget(loadmodel);

        analysis = new QPushButton(horizontalLayoutWidget);
        analysis->setObjectName(QString::fromUtf8("analysis"));

        horizontalLayout_3->addWidget(analysis);

        verticalLayoutWidget_5 = new QWidget(form4);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(320, 430, 102, 131));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        label_9 = new QLabel(verticalLayoutWidget_5);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font2);

        verticalLayout_5->addWidget(label_9);

        label_10 = new QLabel(verticalLayoutWidget_5);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font2);

        verticalLayout_5->addWidget(label_10);

        label_11 = new QLabel(verticalLayoutWidget_5);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font2);

        verticalLayout_5->addWidget(label_11);

        verticalLayoutWidget_6 = new QWidget(form4);
        verticalLayoutWidget_6->setObjectName(QString::fromUtf8("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(430, 430, 141, 131));
        verticalLayout_6 = new QVBoxLayout(verticalLayoutWidget_6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        maichong = new QTextBrowser(verticalLayoutWidget_6);
        maichong->setObjectName(QString::fromUtf8("maichong"));
        maichong->setEnabled(false);

        verticalLayout_6->addWidget(maichong);

        boxing = new QTextBrowser(verticalLayoutWidget_6);
        boxing->setObjectName(QString::fromUtf8("boxing"));
        boxing->setEnabled(false);

        verticalLayout_6->addWidget(boxing);

        guzhang = new QTextBrowser(verticalLayoutWidget_6);
        guzhang->setObjectName(QString::fromUtf8("guzhang"));
        guzhang->setEnabled(false);

        verticalLayout_6->addWidget(guzhang);

        label_12 = new QLabel(form4);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(10, 410, 31, 151));
        label_12->setFont(font1);
        label_12->setFrameShape(QFrame::Box);
        label_13 = new QLabel(form4);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(0, 360, 581, 221));
        label_13->setFrameShape(QFrame::Box);
        widget = new QChartView(form4);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(590, 250, 431, 351));
        trainitems = new QPushButton(form4);
        trainitems->setObjectName(QString::fromUtf8("trainitems"));
        trainitems->setGeometry(QRect(800, 70, 91, 31));
        trainitems->setFont(font);
        traincombo = new QComboBox(form4);
        traincombo->addItem(QString());
        traincombo->addItem(QString());
        traincombo->addItem(QString());
        traincombo->addItem(QString());
        traincombo->addItem(QString());
        traincombo->addItem(QString());
        traincombo->addItem(QString());
        traincombo->setObjectName(QString::fromUtf8("traincombo"));
        traincombo->setGeometry(QRect(900, 70, 81, 31));
        traincombo->setFont(font);
        label_13->raise();
        kuang1->raise();
        verticalLayoutWidget->raise();
        verticalLayoutWidget_2->raise();
        label_5->raise();
        verticalLayoutWidget_3->raise();
        verticalLayoutWidget_4->raise();
        horizontalLayoutWidget->raise();
        verticalLayoutWidget_5->raise();
        verticalLayoutWidget_6->raise();
        label_12->raise();
        widget->raise();
        trainitems->raise();
        traincombo->raise();

        retranslateUi(form4);

        QMetaObject::connectSlotsByName(form4);
    } // setupUi

    void retranslateUi(QWidget *form4)
    {
        form4->setWindowTitle(QCoreApplication::translate("form4", "Form", nullptr));
        label->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275\346\225\260\346\215\256\346\226\207\344\273\2661\357\274\232", nullptr));
        txt_button1->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275txt", nullptr));
        txt1->setText(QString());
        comboBox1->setItemText(0, QCoreApplication::translate("form4", "\346\255\243\345\270\270\346\225\260\346\215\256", nullptr));
        comboBox1->setItemText(1, QCoreApplication::translate("form4", "\345\206\205\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox1->setItemText(2, QCoreApplication::translate("form4", "\345\244\226\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox1->setItemText(3, QCoreApplication::translate("form4", "\346\273\232\345\212\250\344\275\223\346\215\237\344\274\244", nullptr));

        label_button1->setText(QCoreApplication::translate("form4", "\345\210\266\344\275\234\346\240\207\347\255\2761", nullptr));
        lable1->setText(QString());
        clear1->setText(QCoreApplication::translate("form4", "\346\270\205\351\231\244\346\225\260\346\215\2561", nullptr));
        label_2->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275\346\225\260\346\215\256\346\226\207\344\273\2662\357\274\232", nullptr));
        txt_button2->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275txt", nullptr));
        txt2->setText(QString());
        comboBox2->setItemText(0, QCoreApplication::translate("form4", "\346\255\243\345\270\270\346\225\260\346\215\256", nullptr));
        comboBox2->setItemText(1, QCoreApplication::translate("form4", "\345\206\205\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox2->setItemText(2, QCoreApplication::translate("form4", "\345\244\226\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox2->setItemText(3, QCoreApplication::translate("form4", "\346\273\232\345\212\250\344\275\223\346\215\237\344\274\244", nullptr));

        label_button2->setText(QCoreApplication::translate("form4", "\345\210\266\344\275\234\346\240\207\347\255\2762", nullptr));
        lable2->setText(QString());
        clear2->setText(QCoreApplication::translate("form4", "\346\270\205\351\231\244\346\225\260\346\215\2562", nullptr));
        label_3->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275\346\225\260\346\215\256\346\226\207\344\273\2663\357\274\232", nullptr));
        txt_button3->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275txt", nullptr));
        txt3->setText(QString());
        comboBox3->setItemText(0, QCoreApplication::translate("form4", "\346\255\243\345\270\270\346\225\260\346\215\256", nullptr));
        comboBox3->setItemText(1, QCoreApplication::translate("form4", "\345\206\205\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox3->setItemText(2, QCoreApplication::translate("form4", "\345\244\226\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox3->setItemText(3, QCoreApplication::translate("form4", "\346\273\232\345\212\250\344\275\223\346\215\237\344\274\244", nullptr));

        label_button3->setText(QCoreApplication::translate("form4", "\345\210\266\344\275\234\346\240\207\347\255\2763", nullptr));
        lable3->setText(QString());
        clear3->setText(QCoreApplication::translate("form4", "\346\270\205\351\231\244\346\225\260\346\215\2564", nullptr));
        label_4->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275\346\225\260\346\215\256\346\226\207\344\273\2664\357\274\232", nullptr));
        txt_button4->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275txt", nullptr));
        txt4->setText(QString());
        comboBox4->setItemText(0, QCoreApplication::translate("form4", "\346\255\243\345\270\270\346\225\260\346\215\256", nullptr));
        comboBox4->setItemText(1, QCoreApplication::translate("form4", "\345\206\205\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox4->setItemText(2, QCoreApplication::translate("form4", "\345\244\226\345\234\210\346\215\237\344\274\244", nullptr));
        comboBox4->setItemText(3, QCoreApplication::translate("form4", "\346\273\232\345\212\250\344\275\223\346\215\237\344\274\244", nullptr));

        label_button4->setText(QCoreApplication::translate("form4", "\345\210\266\344\275\234\346\240\207\347\255\2764", nullptr));
        lable4->setText(QString());
        clear4->setText(QCoreApplication::translate("form4", "\346\270\205\351\231\244\346\225\260\346\215\2563", nullptr));
        savelabel->setText(QCoreApplication::translate("form4", "\344\277\235\345\255\230\346\240\207\347\255\276", nullptr));
        readlabel->setText(QCoreApplication::translate("form4", "\350\257\273\345\217\226\346\240\267\346\234\254", nullptr));
        trainBPnet->setText(QCoreApplication::translate("form4", "Train", nullptr));
        verification->setText(QCoreApplication::translate("form4", "\351\252\214\350\257\201\346\250\241\345\236\213", nullptr));
        saveModel->setText(QCoreApplication::translate("form4", "\344\277\235\345\255\230\346\250\241\345\236\213", nullptr));
        kuang1->setText(QString());
        label_5->setText(QCoreApplication::translate("form4", "\350\256\255\n"
"\347\273\203\n"
"\347\275\221\n"
"\347\273\234", nullptr));
        label_6->setText(QCoreApplication::translate("form4", "\345\263\255\345\272\246\346\214\207\346\240\207\357\274\232", nullptr));
        label_7->setText(QCoreApplication::translate("form4", "\345\263\260\345\200\274\346\214\207\346\240\207\357\274\232", nullptr));
        label_8->setText(QCoreApplication::translate("form4", "\350\243\225\345\272\246\346\214\207\346\240\207\357\274\232", nullptr));
        readansdata->setText(QCoreApplication::translate("form4", "\350\257\273\345\217\226\346\225\260\346\215\256", nullptr));
        loadmodel->setText(QCoreApplication::translate("form4", "\345\212\240\350\275\275\347\275\221\347\273\234", nullptr));
        analysis->setText(QCoreApplication::translate("form4", "\345\210\206\346\236\220\351\242\204\346\265\213", nullptr));
        label_9->setText(QCoreApplication::translate("form4", "\350\204\211\345\206\262\346\214\207\346\240\207\357\274\232", nullptr));
        label_10->setText(QCoreApplication::translate("form4", "\346\263\242\345\275\242\346\214\207\346\240\207\357\274\232", nullptr));
        label_11->setText(QCoreApplication::translate("form4", "\346\225\205\351\232\234\347\261\273\345\236\213\357\274\232", nullptr));
        label_12->setText(QCoreApplication::translate("form4", "\346\225\205\n"
"\351\232\234\n"
"\351\242\204\n"
"\346\265\213", nullptr));
        label_13->setText(QString());
        trainitems->setText(QCoreApplication::translate("form4", "\350\256\255\347\273\203\346\254\241\346\225\260", nullptr));
        traincombo->setItemText(0, QCoreApplication::translate("form4", "50", nullptr));
        traincombo->setItemText(1, QCoreApplication::translate("form4", "100", nullptr));
        traincombo->setItemText(2, QCoreApplication::translate("form4", "200", nullptr));
        traincombo->setItemText(3, QCoreApplication::translate("form4", "500", nullptr));
        traincombo->setItemText(4, QCoreApplication::translate("form4", "1000", nullptr));
        traincombo->setItemText(5, QCoreApplication::translate("form4", "2000", nullptr));
        traincombo->setItemText(6, QCoreApplication::translate("form4", "5000", nullptr));

    } // retranslateUi

};

namespace Ui {
    class form4: public Ui_form4 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM4_H
